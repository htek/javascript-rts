#Basic Game	

I want to learn more about game development but want to stick with what I know which is web development.
My favorite game is Starcraft 2 and I don't feel there are enough games like it.

I would like to create a basic working prototype of a JavaScript game with an RTS element.

It has to have multiplayer support and the ability to create a community.

This project is open source and if people want to contribute, the more the merrier. Happy coding!